﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KristallTor : MonoBehaviour {

    Animator anima;
    public bool schloss = false;
	// Use this for initialization
	void Start ()
    {
        anima = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void OnTriggerEnter (Collider col)
    {
        if (col.gameObject.tag =="Boat")
        {
            if (schloss == false)
            {
                Debug.Log("im Sutyx");

                anima.Play("Kristalltor");
            }

        }
      
	}

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Boat")
        {
            //schloss = true;
            anima.Play("TorUp");
            
        }
    }
}
