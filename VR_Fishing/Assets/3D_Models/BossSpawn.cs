﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Valve.VR.InteractionSystem
{
    public class BossSpawn : MonoBehaviour
    {

        Animator anima;

        // Use this for initialization
        void Start()
        {
            anima = GetComponent<Animator>();
        }

        // Update is called once per frame
        void OnTriggerEnter(Collider col)
        {
            if (col.gameObject.tag == "Boat")
            {

                

                anima.Play("Boss");
                Invoke("Ende", 3f);

            }

        }

        void Ende()
        {
            SteamVR_Fade.Start(Color.black, 3f);
        }


    }
}
