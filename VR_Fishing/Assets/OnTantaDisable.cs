﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTantaDisable : MonoBehaviour {


    public Quest3 quest3;
	// Use this for initialization
	void Start () {
        quest3 = FindObjectOfType<Quest3>();
	}
	
	// Update is called once per frame
	void OnDisable ()
    {
        quest3.WallDestroyed();
	}
}
