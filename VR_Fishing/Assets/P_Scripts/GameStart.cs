﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{

    public class GameStart : MonoBehaviour
    {

        public GameObject playerCharakter;
        public Transform teleportPoint;
        public Transform tpMainBoat;

        public GameObject PickwickTutorial;

        void Start()
        {
            playerCharakter.transform.position = teleportPoint.position;
            //PickwickTutorial.SetActive(true);
            fadeTpPlayer();
        }

        public void TpPlayer()
        {
            playerCharakter.transform.position = tpMainBoat.position;
            SteamVR_Fade.Start(Color.clear, 2f);

            Invoke("KillMe", 0.5f);
        }

        public void fadeTpPlayer()
        {
            SteamVR_Fade.Start(Color.black, 1f);
            Invoke("TpPlayer", 2f);
        }
        void KillMe()
        {
            Destroy(this.gameObject);
        }
    }
}
