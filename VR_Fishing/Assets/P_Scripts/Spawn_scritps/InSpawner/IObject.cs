﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObject
{

    Action<IObject> EventObjectDeactivated { get; set; }
    GameObject _Objekt { get;  }

    void ActivateObject();

}
