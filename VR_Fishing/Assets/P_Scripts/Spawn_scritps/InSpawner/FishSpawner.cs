﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;

public class FishSpawner : MonoBehaviour
{
    [SerializeField]
    private ObjectPool[] pools;

    [SerializeField]
    private WaveObject[] _spawnWaves;

    [SerializeField]
    private UnityEvent[] spawnEvent;
    public Transform CurrentSpawner;

    [SerializeField]
    private List<IObject> currentWaveObjects;
    [SerializeField]
    private int currentWaveIndex;

    public float SpawnAreaSize;

    private GameObject gunChooser;

    private void Start()
    {
        currentWaveObjects = new List<IObject>();
        StartSpawner();

        //Boat Halt (Fahrunfähig)
        gunChooser = GameObject.Find("WaffenStuff");
        gunChooser.SetActive(false);


    }

    /* private void Update()
     {
        if (Input.GetKeyDown(KeyCode.F)) {StartSpawner();}
         Debug.Log("SpawnStart");
     }
     */
    private void StartSpawner()
    {
        SpawnWave(_spawnWaves[currentWaveIndex]);

    }
    //der GetAwait boi, für die WaveWaitTimer
    
    public Task WaveWaitTimer()
    {
        return Task.Delay(500);
        
    }

    // Asyncron muss nicht alle in einem Frame hintereinander ausgeführt werden. Await
    public async void SpawnWave(WaveObject _object)
    {
        Debug.Log( "Delay " + Time.time );
        await WaveWaitTimer();
        Debug.Log( "nachDelay " +Time.time );
        foreach (SpawnOptions so in _object.spawnOptions)
        {
            for (int i = 0; i < so.amount; i++)
            {
                ObjectPool pool = pools[(int)so.objectPool];
                GameObject currentObject = pool.GetPooledObject();

                //IObject
                IObject currentIObject = currentObject.GetComponent<IObject>();
                currentIObject.EventObjectDeactivated += OnObjectDeactivated;
                currentWaveObjects.Add(currentIObject);
                

                // currentObject.transform.position = currentIObject.transfrom.position.Vector3(UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize), 0f, UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize));
                //CurrentSpawner.transform.position = CurrentSpawner.transform.position; //.Vector3(UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize), 0f, UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize));
                currentObject.transform.position = CurrentSpawner.transform.position + new Vector3(UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize), 0f, UnityEngine.Random.Range(-SpawnAreaSize, SpawnAreaSize)); ;
                if (currentObject.activeSelf == false) {
                    currentObject.SetActive(true);
                }
            }
        }
    }

    private void OnObjectDeactivated(IObject obj)
    {
        obj.EventObjectDeactivated -= OnObjectDeactivated;
        foreach (IObject iobject in currentWaveObjects)
        {
            if (iobject._Objekt.activeSelf == true)
            {
                return;
            }
        }

       currentWaveIndex++;

    // Debug.Log("boi");
    // //Destroy(gameObject);
    // gunChooser.SetActive(true);
    // currentWaveIndex = 0;
    // gameObject.SetActive(false);
    //

        //ausstellung 2 waves Index Problem

         if (currentWaveIndex >= _spawnWaves.Length)
         {


             //gunChooser.SetActive(true);
             Debug.Log("boi");
             //Destroy(gameObject);
             gunChooser.SetActive(true);
             currentWaveIndex = 0;
             gameObject.SetActive(false);

             return;
         }
         SpawnWave(_spawnWaves[currentWaveIndex]);

         
    }

    
}