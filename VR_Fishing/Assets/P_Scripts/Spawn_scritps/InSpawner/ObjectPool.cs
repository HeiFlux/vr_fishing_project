﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour 
{

    //public static ObjectPool current;
    public GameObject pooledObject;
    public int pooledAmount = 5;
    public bool willGrow = false;

    public List<IObject> pooledObjects;

    void Awake()
    {
        //if(current == null)
        //{
        //    current = this;
        //}
        //else
        //{
        //    Destroy(gameObject);
        //}
    }

    void Start()
    {
        pooledObjects = new List<IObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            IObject iObject = obj.GetComponent<IObject>();
            iObject.EventObjectDeactivated += OnObjectDeactivated;


            pooledObjects.Add(iObject);
        }
    }

    private void OnObjectDeactivated(IObject iObject)
    {
   
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i]._Objekt.activeInHierarchy)
            {
                
                return pooledObjects[i]._Objekt;
            }
        }

        if (willGrow)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            pooledObjects.Add(obj.GetComponent<IObject>());
            return obj;
        }
        return null;
    }
}
