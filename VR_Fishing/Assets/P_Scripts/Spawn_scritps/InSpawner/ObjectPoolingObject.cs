﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolingObject : MonoBehaviour, IObject
{
    private Action<IObject> _eventObjectDeactivated = delegate { };
    public Action<IObject> EventObjectDeactivated
    {
        get
        {
            return _eventObjectDeactivated;
        }

        set
        {
            _eventObjectDeactivated = value;
        }
    }

    public GameObject _Objekt
    {
        get
        {
            return gameObject;
        }
    }

    public void ActivateObject()
    {
    }



    private void OnMouseDown()
    {
        GetHit();
    }


    private void GetHit()
    {
        gameObject.SetActive(false);

        EventObjectDeactivated(this);

    }

}
