﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Aktivepoint_spawner : MonoBehaviour {


    


	// Use this for initialization
	void Start () {
        
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "BoatEye")
        {
            gameObject.GetComponent<ParticleSystem>().Play();
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "BoatEye")
        {
            gameObject.GetComponent<ParticleSystem>().Stop();
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
