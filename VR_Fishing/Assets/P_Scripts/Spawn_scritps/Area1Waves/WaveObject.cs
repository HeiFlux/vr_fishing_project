﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaveObject", menuName = "Custom/WaveObject", order = 1)]
public class WaveObject : ScriptableObject
{
    [SerializeField]
    public SpawnOptions[] spawnOptions;
	
}

[System.Serializable]
public struct SpawnOptions
{
    public int amount;
    public SpawnableObject objectPool;
}


public enum SpawnableObject { Fisch1, Fisch2, Fisch3, Fisch4, Boss1};