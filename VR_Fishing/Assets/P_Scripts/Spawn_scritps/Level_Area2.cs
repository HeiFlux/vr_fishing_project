﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Area2 : MonoBehaviour {

   public GameObject[] fish_Spawners;


    // Use this for initialization
    void Start()
    {
        fish_Spawners = GameObject.FindGameObjectsWithTag("Area2");

        for (int i = 0; i < fish_Spawners.Length; i++)
        {
            Debug.Log("Spawner Numbler " + i + " is Named"  +fish_Spawners[i].name);
        }
        foreach (GameObject _obj in fish_Spawners)
        {
            _obj.SetActive(false);
        
        }
    }

    // Update is called once per frame
    void OnTriggerEnter (Collider col) {
		if(col.gameObject.tag == "Boat")
        {
            foreach (GameObject _obj in fish_Spawners)
            {
                _obj.SetActive(true);

            }
        }
	}

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Boat")
        {
            foreach (GameObject _obj in fish_Spawners)
            {
                _obj.SetActive(false);

            }
        }
    }
}
