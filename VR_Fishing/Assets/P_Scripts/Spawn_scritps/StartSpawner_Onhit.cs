﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSpawner_Onhit : MonoBehaviour {
    public GameObject SpawnArea;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            SpawnArea.SetActive(true);
            Destroy(transform.parent.gameObject);
            Debug.Log("OnHitSpawn");
        }
    }

    // Update is called once per frame
    void Update () {
      
    }
}
