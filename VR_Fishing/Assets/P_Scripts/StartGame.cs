﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




namespace Valve.VR.InteractionSystem
{
    public class StartGame : MonoBehaviour
    {

        public GameObject Fisch;
        public bool Go;

        public GameObject hoverButtonStartM;
        public GameObject hoverButtonExitM;


        public float PressTime = 0f;
        public float EndTime = 2f;

        public HoverButton hoverButtonStart;

        public HoverButton hoverButtonExit;
       public AudioSource source;
        public AudioClip shootSound;


        private void Awake()
        {
            source = GetComponent<AudioSource>();
        }
        // Use this for initialization
        private void Start()
        {

           

            hoverButtonStart.onButtonDown.AddListener(OnButtonDownStart);

            hoverButtonExit.onButtonDown.AddListener(OnButtonDownExit);
            
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Debug.Log("down V one");
                
                StartCoroutine(StartBoat());
            }





            if (Input.GetKeyDown(KeyCode.V) && Go == true)
            {
                Debug.Log("down V two");
                

                Go = false;

            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                

            }
        }

        IEnumerator StartBoat()
        {
            hoverButtonExitM.SetActive(false);
            hoverButtonStartM.SetActive(false);
            yield return new WaitForSeconds(EndTime);
            Go = true;
            Fisch.SetActive(true);

            SteamVR_Fade.Start(Color.black, 3f);
            yield return new WaitForSeconds(4);
            source.PlayOneShot(shootSound);
            yield return new WaitForSeconds(5);

            SceneManager.LoadScene(sceneBuildIndex:1);
        }


        private void OnButtonDownStart(Hand hand)
        {
            

            if (Go == false)
            {
                Debug.Log("down V one");
                
                StartCoroutine(StartBoat());
            }

            if (Go == true)
            {
                
                Go = false;
            }
        }



        private void OnButtonDownExit(Hand hand)
        {
            Application.Quit();
        }






    }
}