﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: The bow
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class GunTest : MonoBehaviour
    {
        public SteamVR_Action_Boolean plantAction;
        public GameObject prefabToPlant;
        public ParticleSystem shotEffekt;
        public GameObject hitEffekt;

        public float range = 100f;


        public enum Handedness { Left, Right };

        public Handedness currentHandGuess = Handedness.Left;
        private float timeOfPossibleHandSwitch = 0f;
        private float timeBeforeConfirmingHandSwitch = 1.5f;
        private bool possibleHandSwitch = false;

        public Transform pivotTransform;
        public Transform handleTransform;
        public Transform muzleTransform;

        private Hand hand;

        //ShotReload
        public bool shot = false;
        public float shootTimer;
        public float shootCoolDown = 3f;

        public bool RaySwitch = false;
        public AudioClip shotty;
        

                                     
        SteamVR_Events.Action newPosesAppliedAction;


        //-------------------------------------------------
        private void OnAttachedToHand(Hand attachedHand)
        {
            hand = attachedHand;
        }


        //-------------------------------------------------
        private void HandAttachedUpdate(Hand hand)
        {
            // Reset transform since we cheated it right after getting poses on previous frame
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(33,0,0);

            // Update handedness guess

            var handType = hand.handType;
            if(handType == SteamVR_Input_Sources.LeftHand)
            {
                currentHandGuess = Handedness.Left;
            }
            if (handType == SteamVR_Input_Sources.RightHand)
            {
                currentHandGuess = Handedness.Right;
            }

        }                               
                        
        private void OnEnable()
        {

            if (plantAction == null)
            {
                Debug.LogError("No plant action assigned");
                return;
            }

            plantAction.AddOnChangeListener(OnPlantActionChange, SteamVR_Input_Sources.Any);
        }

        private void OnDisable()
        {
            if (plantAction != null)
            {
                plantAction.RemoveOnChangeListener(OnPlantActionChange, SteamVR_Input_Sources.Any);
            }

        }

        private void OnPlantActionChange(SteamVR_Action_In actionIn)
        {
           // if (plantAction.GetStateDown(SteamVR_Input_Sources.Any))
           // {
           //     Debug.Log("holdTest");
           //     Shoot();
           // }
            //if(hand.grabGripAction)
            //{
            //    Debug.Log("holdTest");
            //    Shoot();
            //}
        }

        public void ShootRight()
        {
            if(currentHandGuess == Handedness.Right)
            {
                Shoot();
            }
        }
        public void ShootLeft()
        {
            if (currentHandGuess == Handedness.Left)
            {
                Shoot();
            }
        }


        public void Shoot()
        {
            

            if (!shot)
            {


                GameObject obj = BulletObjectPool.current.GetPooledObject();
                if (obj == null) return;

                obj.transform.position = muzleTransform.position;
                obj.transform.rotation = transform.rotation;
                obj.GetComponent<Rigidbody>().velocity = 35 * transform.localScale.x * obj.transform.forward;
                Debug.Log("triedShooting");
                obj.SetActive(true);

                shotEffekt.Play();

                shootTimer = shootCoolDown;
                shot = true;
                
                
                // Muss 1 mal abspielen, nicht mehr
                hand.TriggerHapticPulse((ushort)8000);

            }
        }
        private void Update()
        {
            if(shot)
            {
                if (shootTimer > 0)
                {
                    Debug.Log("Zählt");
                    shootTimer -= Time.deltaTime;
                }
                else
                {
                    shot = false;
                }
            }
        }


        //-------------------------------------------------
        private void OnHandFocusLost(Hand hand)
        {
            gameObject.SetActive(false);
        }


        //-------------------------------------------------
        private void OnHandFocusAcquired(Hand hand)
        {
            gameObject.SetActive(true);
            OnAttachedToHand(hand);
        }


        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            hand.TriggerHapticPulse(4000);

            Destroy(gameObject);
        }


        //-------------------------------------------------
        
    }
}