﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NearestRadar : MonoBehaviour {

    private Vector3 targetDir;

    public float speed;

    public bool lookForEnemies = false;

    





    void Update()
    {
        if (lookForEnemies)
        {
            FindClosestEnemy();
        }


    }
    void FindClosestEnemy()
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        Enemy closestEnemy = null;
        Enemy[] allEnemies = GameObject.FindObjectsOfType<Enemy>();

        foreach (Enemy currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if(distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;
            }
        }
        Debug.DrawLine(this.transform.position, closestEnemy.transform.position);
        float step = speed * Time.deltaTime;     

        //transform.rotation = Quaternion.LookRotation(closestEnemy.transform.position);
        transform.LookAt(closestEnemy.transform.position);
    }

    public void StopSearch()
    {
        lookForEnemies = false;
    }
    public void BeginSearch()
    {
        lookForEnemies = true;
    }


}
