﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogAnimationCues : MonoBehaviour {

    public LootDropper lootDropper;

    public Animator anim;

    // Use this for initialization
    void Start () {
        lootDropper = FindObjectOfType<LootDropper>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void BeginDroppingLoot()
    {
        lootDropper.DropLoot();
    }
    public void AnimateLootDrop()
    {
        anim.Play("GetItem");
    }
}
