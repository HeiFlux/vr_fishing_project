﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarTest : MonoBehaviour {

    public Transform area1;
    public Transform area2;
    public Transform area3;

    private Vector3 targetDir;

    public float speed;

    void Start()
    {
       // Vector3 targetDir = area1.position - transform.position;

    }

        // Update is called once per frame
        void Update ()
    {
        float distanceToArea1 = Vector3.Distance(area1.position, transform.position);
        float distanceToArea2 = Vector3.Distance(area2.position, transform.position);
        float distanceToArea3 = Vector3.Distance(area3.position, transform.position);

        Vector3 dirArea1 = area1.position - transform.position;
        Vector3 dirArea2 = area2.position - transform.position;
        Vector3 dirArea3 = area3.position - transform.position;


        Debug.DrawRay(transform.position, dirArea1, Color.red);
        Debug.DrawRay(transform.position, dirArea2, Color.red);
        Debug.DrawRay(transform.position, dirArea3, Color.red);


        if(distanceToArea1 >= distanceToArea2)
        {
            Debug.Log("A2");
            targetDir = dirArea2;
        }
        else
        {
            Debug.Log("A1");
            targetDir = dirArea1;
        }





        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, distanceToArea1);
        Debug.DrawRay(transform.position, newDir, Color.green);

        transform.rotation = Quaternion.LookRotation(newDir);

    }
}
