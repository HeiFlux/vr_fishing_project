﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Valve.VR.InteractionSystem
{
    public class Fadestart : MonoBehaviour
    {

        // Use this for initialization
        void Awake()
        {
            SteamVR_Fade.Start(Color.black, 0f);
        }

        // Update is called once per frame
        void Update()
        {
            
            SteamVR_Fade.Start(Color.clear, 5f);
        }
    }
}