﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickwickAudioManager : MonoBehaviour {

    public AudioClip sound_001;
    public AudioClip sound_002;
    public AudioClip sound_003;
    public AudioClip sound_004;
    public AudioClip sound_005;
    public AudioClip sound_006;
    public AudioClip sound_007;
    public AudioClip sound_008;

    public AudioSource pickwickAudio;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Play01()
    {
        pickwickAudio.PlayOneShot(sound_001);
        Invoke("Play02", 11f);
    }
    public void Play02()
    {
        pickwickAudio.PlayOneShot(sound_002);
        Invoke("Play03", 26.7f);
    }
    public void Play03()
    {
        pickwickAudio.PlayOneShot(sound_003);
        Invoke("Play04", 19.2f);
    }
    public void Play04()
    {
        pickwickAudio.PlayOneShot(sound_004);
    }
    public void Play05()
    {
        pickwickAudio.PlayOneShot(sound_005);
    }
    public void Play06()
    {
        pickwickAudio.PlayOneShot(sound_006);
    }
    public void Play07()
    {
        pickwickAudio.PlayOneShot(sound_007);
    }
    public void Play08()
    {
        pickwickAudio.PlayOneShot(sound_008);
    }
}
