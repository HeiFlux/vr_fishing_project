﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Quest3 : MonoBehaviour {

    public UnityEvent QuestStart;
    public UnityEvent QuestEnd;
    public UnityEvent MidQuestEnter;
    public UnityEvent MidQuestExit;

    public Animator pickwickAnim;
    public Pickwick_Controller pickCon;

    public bool questLäuft;
    public bool playerDa;

    public bool wallGone = false;
    public PickwickAudioManager audioPlay;

    void Start()
    {
        pickCon = FindObjectOfType<Pickwick_Controller>();
        audioPlay = FindObjectOfType<PickwickAudioManager>();

    }

    public void BeginQuest()
    {

        Invoke("StartThisQuest", 31f);
        audioPlay.Play06();
        Debug.Log("Start Dialog Quest 3");
    }

    public void StartThisQuest()
    {
        QuestStart.Invoke();
        pickCon.Set_Quest1();
        questLäuft = true;
    }
    public void EndThisQuest()
    {
        QuestEnd.Invoke();
        questLäuft = false;
    }

    void Update()
    {
        

        if (!playerDa)
        {
            pickwickAnim.SetBool("Idling", true);
        }
        else
        {
            pickwickAnim.SetBool("Idling", false);
        }

        if (wallGone)
        {
            EndThisQuest();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestEnter.Invoke();
            playerDa = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestExit.Invoke();
            playerDa = false;
        }
    }

    public void WallDestroyed()
    {
        EndThisQuest();
    }
    private void OnEnable()
    {
        audioPlay = FindObjectOfType<PickwickAudioManager>();
    }
}
