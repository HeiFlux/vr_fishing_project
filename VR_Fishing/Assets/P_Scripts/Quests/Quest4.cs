﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Quest4 : MonoBehaviour {

    public UnityEvent QuestStart;
    public UnityEvent QuestEnd;
    public UnityEvent MidQuestEnter;
    public UnityEvent MidQuestExit;





    public MerchantInventory inventory;
    public Animator pickwickAnim;
    public Pickwick_Controller pickCon;

    public bool questLäuft = false;
    public bool playerDa;
    public bool playerdetection = false;

    public PickwickAudioManager audioPlay;

    void Start()
    {
        pickCon = FindObjectOfType<Pickwick_Controller>();
        inventory = FindObjectOfType<MerchantInventory>();
        audioPlay = FindObjectOfType<PickwickAudioManager>();

    }

    public void BeginQuest()
    {

        Invoke("StartThisQuest", 20f);
        audioPlay.Play07();
        Debug.Log("Start Dialog");
        playerdetection = false;
        inventory = FindObjectOfType<MerchantInventory>();

    }

    public void StartThisQuest()
    {
        QuestStart.Invoke();
        pickCon.Set_Quest1();
        questLäuft = true;
        inventory = FindObjectOfType<MerchantInventory>();

    }
    public void EndThisQuest()
    {
        QuestEnd.Invoke();
        questLäuft = false;
        Debug.Log("Quest4End");
    }

    void Update()
    {
        inventory = FindObjectOfType<MerchantInventory>();


        if (!playerDa)
        {
            pickwickAnim.SetBool("Idling", true);
        }
        else
        {
            pickwickAnim.SetBool("Idling", false);
        }

        if (inventory.ZappyfishCount >= 3 && inventory.HomingFishCount >= 3 && questLäuft)
        {
            EndThisQuest();
            questLäuft = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BoatEye" && playerdetection)
        {
            BeginQuest();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestEnter.Invoke();
            playerDa = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestExit.Invoke();
            playerDa = false;
        }
    }
    public void PlayerDetectionStart()
    {
        playerdetection = true;
    }
}
