﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pickwick_Controller : MonoBehaviour
{

    public bool PreStart = false;
    public bool Quest_1 = false;
    public bool Quest_2 = false;
    public bool Quest_3 = false;
    public bool Quest_4_5 = false;
    public bool Quest_6 = false;

    public bool PickwickWaiting = false;
    public bool BoatSeen = false;

    public Animator pickWickAnimator;



    // Use this for initialization
    void Awake()
    {
        //PreStart = false;
        //Quest_1 = false;
        //Quest_2 = false;
        //Quest_3 = false;
        //Quest_4_5 = false;
        //Quest_6 = false;

        Invoke("Set_Quest1", 3f);
   }
    

    public void Set_PreStart()
        {
            PreStart = true;

        Quest_2 = false;
        Quest_3 = false;
        Quest_4_5 = false;
        Quest_6 = false;
        PreStart = false;
    }
        public void Set_Quest1()
        {        
        Quest_1 = true;

        Quest_2 = false;
        Quest_3 = false;
        Quest_4_5 = false;
        Quest_6 = false;
        PreStart = false;
    }
        public void Set_Quest2()
        {
        Quest_2 = true;
        

        Quest_1 = false;
        Quest_3 = false;
        Quest_4_5 = false;
        Quest_6 = false;
        PreStart = false;
    }
        public void Set_Quest3()
        {

        }
        public void Set_Quest4_5()
        {

        }
        public void Set_Quest6()
        {

        }
        public void Set_End()
        {

        }

        public void Pick_Set_Waiting()
        {
            PickwickWaiting = true;
        }

        private void OnTriggerStay(Collider other)
        {
            //if (other.tag == "Boat" && PickwickWaiting)
            //{
            //    pickWickAnimator.SetBool("Give Bucket", true);
            //    
            //
            //}
        }
        private void OnTriggerExit(Collider other)
        {
            //if (other.tag == "Boat")
            //{
            //    pickWickAnimator.SetBool("Idle", true);
            //}
        }
    }

