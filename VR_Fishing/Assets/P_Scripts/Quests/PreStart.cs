﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class PreStart : MonoBehaviour {


    public UnityEvent QuestStart;
    public UnityEvent QuestEnd;

    public MerchantInventory inventory;

  

    public void StartThisQuest()
    {
        QuestStart.Invoke();
    }
    public void EndThisQuest()
    {
        QuestEnd.Invoke();
    }

    private void Update()
    {
        if(inventory.BomberfishCount >= 1) { EndThisQuest(); }
    }
}
