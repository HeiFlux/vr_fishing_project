﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestQuest : MonoBehaviour {

    public MerchantInventory inventory;
    public Gun_Chooser gun;

    public GameObject NextPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(inventory.JumpyFishCount >= 3)
        {
            inventory.JumpyFishCount = inventory.JumpyFishCount -3;
            gun.SetShot();
            NextPoint.SetActive(true);
        }
	}
}
