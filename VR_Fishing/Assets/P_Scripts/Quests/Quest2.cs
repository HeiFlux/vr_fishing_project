﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Quest2 : MonoBehaviour {

    public UnityEvent QuestStart;
    public UnityEvent QuestEnd;
    public UnityEvent MidQuestEnter;
    public UnityEvent MidQuestExit;





    public MerchantInventory inventory;
    public Animator pickwickAnim;
    public Pickwick_Controller pickCon;

    public bool questLäuft;
    public bool playerDa;

    public PickwickAudioManager audioPlay;

    void Start()
    {
        pickCon = FindObjectOfType<Pickwick_Controller>();
        inventory = FindObjectOfType<MerchantInventory>();
        audioPlay = FindObjectOfType<PickwickAudioManager>();

    }

    public void BeginQuest()
    {

        Invoke("StartThisQuest", 21.5f);
        audioPlay.Play05();
        Debug.Log("Start Dialog");
    }

    public void StartThisQuest()
    {
        QuestStart.Invoke();
        pickCon.Set_Quest1();
        questLäuft = true;
    }
    public void EndThisQuest()
    {
        QuestEnd.Invoke();
        questLäuft = false;
        Debug.Log("Quest2 end");
    }

    void Update()
    {
       

        if (!playerDa)
        {
            pickwickAnim.SetBool("Idling", true);
        }
        else
        {
            pickwickAnim.SetBool("Idling", false);
        }

        if (inventory.BossCount >= 1)
        {
            EndThisQuest();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestEnter.Invoke();
            playerDa = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestExit.Invoke();
            playerDa = false;
        }
    }
}