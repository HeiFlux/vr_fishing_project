﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Quest1 : MonoBehaviour {

    public UnityEvent BeforeQuest;

    public UnityEvent QuestStart;
    public UnityEvent QuestEnd;
    public UnityEvent MidQuestEnter;
    public UnityEvent MidQuestExit;


    public int Needed_Bomber = 3;
    public int Needed_Jumpy = 3;


    public MerchantInventory inventory;
    public Animator pickwickAnim;
    public Pickwick_Controller pickCon;
    public PickwickAudioManager audioPlay;

    public bool questLäuft = false;
    public bool playerDa;

    void Start()
    {

        pickCon = FindObjectOfType<Pickwick_Controller>();
        inventory = FindObjectOfType<MerchantInventory>();
        audioPlay = FindObjectOfType<PickwickAudioManager>();
        Invoke("QuestStartOffset", 10f);

    }

    void QuestStartOffset()
    {
        audioPlay.Play01();
        BeforeQuest.Invoke();
        Invoke("BeginQuest", 68.6f);
        pickwickAnim.SetBool("Idling", true);
    }


    void BeginQuest()
    {

        StartThisQuest();
        Debug.Log("Start Dialog");
    }

    public void StartThisQuest()
    {
        QuestStart.Invoke();
        pickCon.Set_Quest1();
        questLäuft= true;
    }
    public void EndThisQuest()
    {
        QuestEnd.Invoke();
        questLäuft = false;
        
    }

    void Update()
    {
        

        if(!playerDa)
        {
            
        }
        else
        {
            pickwickAnim.SetBool("Idling", false);
        }

        if (inventory.BomberfishCount >= Needed_Bomber && inventory.JumpyFishCount >= Needed_Jumpy )
        {
            EndThisQuest();
        }
        if (Input.GetKeyDown("space")&& !questLäuft)
        {
            audioPlay.pickwickAudio.Stop();
            
            audioPlay.CancelInvoke();
            CancelInvoke();
            BeginQuest();
        
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "BoatEye" && questLäuft)
        {
            MidQuestEnter.Invoke();
            playerDa = true;
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BoatEye" && questLäuft)
        {
            MidQuestExit.Invoke();
            playerDa = false;
            audioPlay.pickwickAudio.Stop();
        }
    }
    
}
