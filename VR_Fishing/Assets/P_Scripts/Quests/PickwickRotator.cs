﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickwickRotator : MonoBehaviour {

    public bool startFollow;

    private GameObject  lookPos;

    void Start()
    {
        lookPos = GameObject.Find("TargetPoint");

    }
    void Update()
    {
        if (startFollow)
        {
            transform.LookAt(lookPos.transform.position);
        }
    }
    public void StartFollow()
    {
        startFollow = true;
    }
    public void StopFollow()
    {
        startFollow = false;
    }
}
