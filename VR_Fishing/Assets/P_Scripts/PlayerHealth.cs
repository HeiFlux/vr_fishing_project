﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{

    public class PlayerHealth : MonoBehaviour
    {

        public GameObject[] enemies;
        GameObject itemToDeakti;
        public int CurrentHealth = 0;
        int FullHealth;
        public float DmgDistanc;
        float Normaldis;
        GameObject OnAttack;
        public Transform Spawn;
        public GameObject Boat;

        public LootDropper lootDropper;


        // GameObject Spawnernest;
        //  public GameObject[] Spawnernests;

        // Use this for initialization
        void Start()
        {
            FullHealth += CurrentHealth;
            Normaldis += DmgDistanc;
            OnAttack = this.gameObject;
            lootDropper = FindObjectOfType<LootDropper>();

        }

        // Update is called once per frame
        void Update()
        {

            enemies = GameObject.FindGameObjectsWithTag("Fish");
           // Spawnernests = GameObject.FindGameObjectsWithTag("Spawner");
            foreach (GameObject enemie in enemies)

            {
                
                float distance = Vector3.Distance(OnAttack.transform.position, enemie.transform.position);
                if (distance <= DmgDistanc)
                {

                    

                    itemToDeakti = enemie;
                    CurrentHealth -= 1;
                    itemToDeakti.SetActive(false);
                    return;
                    
                }

            }

        //    foreach (GameObject Spawneran in Spawnernests)
        //    {
        //        Spawnernest = Spawneran;
        //    }

            if (CurrentHealth == 0)
            {
                SteamVR_Fade.Start(Color.black, 3f);
                Debug.Log("Dead2");
                Invoke("TpPlayerDeath", 5f);
                DmgDistanc += 80f;
                lootDropper.ResetDropCounts();
               // Spawnernest.SetActive(false);

            }

            
        }
        public void TpPlayerDeath() {
            Boat.transform.position = Spawn.position;
            SteamVR_Fade.Start(Color.clear, 4f);
            CurrentHealth = FullHealth;
            DmgDistanc = Normaldis;
            
        }
    }
}
