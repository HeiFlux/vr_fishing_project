﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: The bow
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class RayGunTest : MonoBehaviour
    {
        public SteamVR_Action_Boolean plantAction;
        public GameObject prefabToPlant;
        public GameObject shotEffekt;
        public GameObject hitEffekt;

        public float range = 100f;


        public enum Handedness { Left, Right };

        public Handedness currentHandGuess = Handedness.Left;
        private float timeOfPossibleHandSwitch = 0f;
        private float timeBeforeConfirmingHandSwitch = 1.5f;
        private bool possibleHandSwitch = false;

        public Transform pivotTransform;
        public Transform handleTransform;
        public Transform muzleTransform;

        private Hand hand;

        //ShotReload
        public bool rshot = false;
        public float rshootTimer;
        private float rshootCoolDown = 3f;


        SteamVR_Events.Action newPosesAppliedAction;


        //-------------------------------------------------
        private void OnAttachedToHand(Hand attachedHand)
        {
            hand = attachedHand;
        }


        //-------------------------------------------------
        private void HandAttachedUpdate(Hand hand)
        {
            // Reset transform since we cheated it right after getting poses on previous frame
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(33, 0, 0);

            // Update handedness guess

        }

        private void OnEnable()
        {

            if (plantAction == null)
            {
                Debug.LogError("No plant action assigned");
                return;
            }

            plantAction.AddOnChangeListener(OnPlantActionChange, SteamVR_Input_Sources.RightHand);
        }

        private void OnDisable()
        {
            if (plantAction != null)
            {
                plantAction.RemoveOnChangeListener(OnPlantActionChange, SteamVR_Input_Sources.RightHand);
            }

        }

        private void OnPlantActionChange(SteamVR_Action_In actionIn)
        {
            if (plantAction.GetStateDown(SteamVR_Input_Sources.RightHand))
            {
                Shoot();
                Debug.Log("hold");
            }
        }




        public void Shoot()
        {
            if (rshot == false)
            {
                RaycastHit hit;
                if (Physics.Raycast(muzleTransform.transform.position, muzleTransform.transform.forward, out hit, range))
                {
                    Debug.Log(hit.transform.name);
                    Instantiate(hitEffekt, hit.point, Quaternion.LookRotation(hit.normal));

                }

                //StartCoroutine(DoPlant());
               // GameObject shooting = GameObject.Instantiate<GameObject>(prefabToPlant, muzleTransform.position, transform.rotation);


                //shooting.GetComponent<Rigidbody>().velocity = 35 * transform.localScale.x * shooting.transform.forward;

                GameObject shootingEffekt = GameObject.Instantiate<GameObject>(shotEffekt, muzleTransform.position, transform.rotation);

                rshootTimer = rshootCoolDown;
                rshot = true;
                

                // Muss 1 mal abspielen, nicht mehr
                //hand.TriggerHapticPulse((ushort)8000);

            }
        }
        private void Update()
        {
            if (rshot)
            {
                //rshootTimer -= Time.deltaTime;
                //StartCoroutine(GunCoolDown());
                if (rshootTimer >= 0f)
                {
                    Debug.Log("Zählt");
                    rshootTimer -= Time.deltaTime;
                }
                
                if (rshootTimer <= 0f)
                {
                   rshot = false;
                    
                }
            }
        }

        //IEnumerator GunCoolDown()
        //{
        //    
        //    yield return new WaitForSeconds(shootCoolDown);
        //    hand.TriggerHapticPulse((ushort)200);
        //    shot = false;
        //}
        ////-------------------------------------------------
        private void OnHandFocusLost(Hand hand)
        {
            gameObject.SetActive(false);
            
        }


        //-------------------------------------------------
        private void OnHandFocusAcquired(Hand hand)
        {
            gameObject.SetActive(true);
            OnAttachedToHand(hand);
        }


        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            Destroy(gameObject);
        }


        //-------------------------------------------------

    }
}