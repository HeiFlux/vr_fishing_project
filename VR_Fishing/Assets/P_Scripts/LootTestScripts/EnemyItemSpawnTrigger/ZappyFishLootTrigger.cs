﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZappyFishLootTrigger : MonoBehaviour {

    public LootChances lootChances;

    void Awake()
    {
        lootChances = FindObjectOfType<LootChances>();

    }



    void OnDisable()
    {
        Debug.Log("Tried to Animate LootDropp");
        lootChances.ZappyFischKilled();
    }
}
