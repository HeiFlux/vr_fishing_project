﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeTest : MonoBehaviour {

    public MerchantInventory merchant;

    public Transform spawnTransform;
    public GameObject spawnObject;

    private int reqFish1 = 3;
    private int reqFish2 = 4;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(merchant.BomberfishCount == reqFish1 && merchant.ZappyfishCount == reqFish2)
        {
            Spawn();
        }
	}

    void Spawn()
    {
        GameObject shooting = GameObject.Instantiate<GameObject>(spawnObject, spawnTransform.position, transform.rotation);
        Destroy(gameObject);
    }
}
