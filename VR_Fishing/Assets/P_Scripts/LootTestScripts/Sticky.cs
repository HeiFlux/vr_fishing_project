﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sticky : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Stashable")
        {
            other.transform.parent = this.transform.parent;
            other.GetComponent<Rigidbody>().isKinematic = true;
            
        }
    }

   
}
