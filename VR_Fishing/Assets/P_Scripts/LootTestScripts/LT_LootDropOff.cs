﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LT_LootDropOff : MonoBehaviour {

    public MerchantInventory merchant;
    public ParticleSystem effect;
    public ParticleSystem effect2;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Got Object");
        if(col.gameObject.name == "BomberFishItem"|| col.gameObject.name == "BomberFishItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.BomberfishCount ++;
            effect.Play();
            effect2.Play();

        }
        if (col.gameObject.name == "JumpyFishItem" || col.gameObject.name == "JumpyFishItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.JumpyFishCount++;
            effect.Play();
            effect2.Play();


        }
        if (col.gameObject.name == "ZappFishItem" || col.gameObject.name == "ZappFishItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.ZappyfishCount++;
            effect.Play();
            effect2.Play();


        }
        if (col.gameObject.name == "HomingFishItem" || col.gameObject.name == "HomingFishItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.HomingFishCount++;
            effect.Play();
            effect2.Play();


        }
        if (col.gameObject.name == "ExtraItem" || col.gameObject.name == "ExtraItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.ExtraItemCount++;
            effect.Play();
            effect2.Play();


        }
        if (col.gameObject.name == "BossItem" || col.gameObject.name == "BossItem(Clone)")
        {
            Debug.Log("Got " + col.name);
            Destroy(col.gameObject);
            merchant.BossCount++;
            effect.Play();
            effect2.Play();


        }

    }
}
