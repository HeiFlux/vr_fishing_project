﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootDropper : MonoBehaviour {

    public int BomberFisch_Dropcount;
    public int JumpyFisch_Dropcount;
    public int BossFetti_Dropcount;
    public int ZappyFisch_Dropcount;
    public int HomingFisch_Dropcount;
    public int ExtraItem_Dropcount;

    public GameObject BomberfischItem;
    public GameObject JumpyfischItem;
    public GameObject BossFettiItem;
    public GameObject ZappyfischItem;
    public GameObject HomingFischItem;
    public GameObject ExtraItem;


    public bool dropable = false;

    public GameObject dropPoint;
    
    private void Start()
    {
        Invoke("ResetDropCounts", 0.3f);
        dropPoint = GameObject.Find("TargetPoint");
       
    }

    private void Update()
    {
        
    }





    // Use this for initialization
    public void DropLoot ()
    {
        for(int i = 0; i < BomberFisch_Dropcount; i++)
        {
            Instantiate(BomberfischItem,  dropPoint.transform.position, Quaternion.identity);
        }
        for (int i = 0; i < JumpyFisch_Dropcount; i++)
        {
            Instantiate(JumpyfischItem, dropPoint.transform.position, Quaternion.identity);
        }
        for (int i = 0; i < BossFetti_Dropcount; i++)
        {
            Instantiate(BossFettiItem, dropPoint.transform.position, Quaternion.identity);
        }
        for (int i = 0; i < ZappyFisch_Dropcount; i++)
        {
            Instantiate(ZappyfischItem, dropPoint.transform.position, Quaternion.identity);
        }
        for (int i = 0; i < HomingFisch_Dropcount; i++)
        {
            Instantiate(HomingFischItem, dropPoint.transform.position, Quaternion.identity);
        }
        for (int i = 0; i < ExtraItem_Dropcount; i++)
        {
            Instantiate(ExtraItem, dropPoint.transform.position, Quaternion.identity);
        }

        BomberFisch_Dropcount = 0;
        JumpyFisch_Dropcount = 0;
        BossFetti_Dropcount = 0;
        ZappyFisch_Dropcount = 0;
        HomingFisch_Dropcount = 0;
        ExtraItem_Dropcount = 0;
    }
    public void ResetDropCounts()
    {
        BomberFisch_Dropcount = 0;
        JumpyFisch_Dropcount = 0;
        BossFetti_Dropcount = 0;
        ZappyFisch_Dropcount = 0;
        HomingFisch_Dropcount = 0;
        ExtraItem_Dropcount = 0;
    }


}
