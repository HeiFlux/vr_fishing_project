﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LT_FishSpawner : MonoBehaviour {

    public Transform spawnTransform;
    public GameObject fish1;
    public GameObject fish2;
    public GameObject fish3;
    public GameObject fish4;
    public GameObject fish5;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	public void SpawnFish1 () {
        GameObject shooting = GameObject.Instantiate<GameObject>(fish1, spawnTransform.position, transform.rotation);
    }
    public void SpawnFish2()
    {
        GameObject shooting = GameObject.Instantiate<GameObject>(fish2, spawnTransform.position, transform.rotation);
    }
    public void SpawnFish3()
    {
        GameObject shooting = GameObject.Instantiate<GameObject>(fish3, spawnTransform.position, transform.rotation);
    }
    public void SpawnFish4()
    {
        GameObject shooting = GameObject.Instantiate<GameObject>(fish4, spawnTransform.position, transform.rotation);
    }
    public void SpawnFish5()
    {
        GameObject shooting = GameObject.Instantiate<GameObject>(fish5, spawnTransform.position, transform.rotation);
    }
}
