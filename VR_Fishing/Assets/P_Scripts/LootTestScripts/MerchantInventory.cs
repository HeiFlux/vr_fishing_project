﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MerchantInventory : MonoBehaviour {

    public int BomberfishCount;
    public int JumpyFishCount;
    public int BossCount;
    public int ZappyfishCount;
    public int HomingFishCount;
    public int ExtraItemCount;


    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;
    public Text text5;

    // Use this for initialization
    public void EmptyInventory()
    {
        BomberfishCount = 0;
        JumpyFishCount = 0;
        BossCount = 0;
        ZappyfishCount = 0;
        HomingFishCount = 0;
        ExtraItemCount = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //text1.text = BomberfishCount.ToString();
        //text2.text = JumpyFishCount.ToString();
        //text3.text = BossCount.ToString();
        //text4.text = ZappyfishCount.ToString();
        //text5.text = HomingFishCount.ToString();

    }
}
