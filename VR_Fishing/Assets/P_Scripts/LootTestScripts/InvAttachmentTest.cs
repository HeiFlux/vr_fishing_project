﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvAttachmentTest : MonoBehaviour
{
    public int canTakeThisMuch;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Stashable" && this.transform.childCount <= 1+canTakeThisMuch)
        {
            col.transform.parent = this.transform;
            col.GetComponent<Rigidbody>().isKinematic = true;
            col.transform.position = this.transform.position;
            col.transform.rotation = this.transform.rotation;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Stashable" && this.transform.childCount <= 1+canTakeThisMuch)
        {
            if(col.transform.parent == null)
            {
                col.transform.parent = this.transform;
                col.SendMessage("DetachNow");
                col.GetComponent<Rigidbody>().isKinematic = true;
                col.transform.position = this.transform.position;
                col.transform.rotation = this.transform.rotation;
            }
            
            
        }
    }
    
}
