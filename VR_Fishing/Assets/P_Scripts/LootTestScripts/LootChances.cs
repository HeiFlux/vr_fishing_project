﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootChances : MonoBehaviour {

    public LootDropper lootDropper;

    [Range(0.0f,1.0f)]
    public float Bomberfisch_Chances;

    [Range(0.0f, 1.0f)]
    public float Jumpyfisch_Chances;

    [Range(0.0f, 1.0f)]
    public float Boss_Fetti_Chances;

    [Range(0.0f, 1.0f)]
    public float ZappyFisch_Chances;

    [Range(0.0f, 1.0f)]
    public float HomingFisch_Chances;

    [Range(0.0f, 1.0f)]
    public float ExtraItem_Chances;

    // Use this for initialization
    void Start ()
    {
        lootDropper = FindObjectOfType<LootDropper>();
	}
	
	

    public void BomberfischKilled()
    {
        float randValue = Random.value;
        if (randValue < Bomberfisch_Chances) // 45% of the time
        {
            lootDropper.BomberFisch_Dropcount = lootDropper.BomberFisch_Dropcount+1;
        }
    }
    public void JumpyFischKilled()
    {
        float randValue = Random.value;
        if (randValue < Jumpyfisch_Chances) // 45% of the time
        {
            lootDropper.JumpyFisch_Dropcount = lootDropper.JumpyFisch_Dropcount + 1;
        }
    }
    public void Boss_FettiKilled()
    {
        
            lootDropper.BossFetti_Dropcount = lootDropper.BossFetti_Dropcount + 1;
        
    }
    public void ZappyFischKilled()
    {
        float randValue = Random.value;
        if (randValue < Boss_Fetti_Chances) // 45% of the time
        {
            lootDropper.ZappyFisch_Dropcount = lootDropper.ZappyFisch_Dropcount + 1;
        }
    }
    public void HomingFischKilled()
    {
        float randValue = Random.value;
        if (randValue < Boss_Fetti_Chances) // 45% of the time
        {
            lootDropper.HomingFisch_Dropcount = lootDropper.HomingFisch_Dropcount + 1;
        }
    }
    public void ExtraItemChance()
    {
        float randValue = Random.value;
        if (randValue < Boss_Fetti_Chances) // 45% of the time
        {
            lootDropper.ExtraItem_Dropcount = lootDropper.ExtraItem_Dropcount + 1;
        }
    }
}
