﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTelepoter : MonoBehaviour {

    public GameObject playerCharakter;
    public Transform teleportPoint;

	public void TPplayer()
    {
        playerCharakter.transform.position = teleportPoint.position;
    }
}
