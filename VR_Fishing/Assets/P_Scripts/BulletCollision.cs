﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour {

    public GameObject waterShotEffekt;
    public GameObject fishShotEffekt;

    


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 0.4f))
        {
            Debug.Log(hit.transform.name);
           //GameObject obj = SplashEffektObjectPool.current.GetPooledObject();
           //if (obj == null) return;
           //
           //obj.transform.position = hit.point;
           //obj.transform.rotation = Quaternion.LookRotation(hit.normal);
           //
           //Debug.Log("triedHitting");
           //obj.SetActive(true);

            if(hit.collider.tag == "Fish")
            {
                

                GameObject fishEffekt = GameObject.Instantiate<GameObject>(fishShotEffekt, transform.position, Quaternion.identity);
               //hit.collider.gameObject.SetActive(false);
                gameObject.SetActive(false);
                hit.collider.gameObject.SendMessageUpwards("GetBulletHit");
            }
            if (hit.collider.tag == "BoatBoddy")
            {
                Debug.Log(hit.transform.name);
                GameObject obj = SplashEffektObjectPool.current.GetPooledObject();
                if (obj == null) return;

                obj.transform.position = hit.point;
                obj.transform.rotation = Quaternion.LookRotation(hit.normal);

                Debug.Log("triedHitting");
                obj.SetActive(true);

            }


            //gameObject.SetActive(false);

        }
    }

    //void OnTriggerEnter(Collider col)
    //{
    //    if (col.gameObject)
    //    {
    //        RaycastHit hit;
    //        if (Physics.Raycast(transform.position, transform.forward, out hit, 0.5f))
    //        {
    //            Debug.Log(hit.transform.name);
    //            GameObject obj = SplashEffektObjectPool.current.GetPooledObject();
    //            if (obj == null) return;
    //
    //            obj.transform.position = hit.point;
    //            obj.transform.rotation = Quaternion.LookRotation(hit.normal);
    //            
    //            Debug.Log("triedHitting");
    //            obj.SetActive(true);
    //
    //        }
    //        
    //        Debug.Log("Hit");
    //
    //        if(col.tag == "Fish")
    //        {
    //            GameObject fishEffekt = GameObject.Instantiate<GameObject>(fishShotEffekt, transform.position, Quaternion.identity);
    //            Destroy(col.gameObject);
    //
    //        }
    //        gameObject.SetActive(false);
    //    }
    //}

    void SpawnEffekt()
    {

    }
}
