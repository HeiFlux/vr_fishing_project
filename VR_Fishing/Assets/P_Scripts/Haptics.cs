﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


namespace Valve.VR.InteractionSystem
{
    public class Haptics : MonoBehaviour
    {

        public SteamVR_Action_Vibration hapticAction;
        public SteamVR_Action_Boolean trackpadAction;

        private bool shot = false;
        private float shootTimer;
        private float shootCoolDown = 0.1f;


        // Update is called once per frame
        void Update()
        {
            if(trackpadAction.GetStateDown(SteamVR_Input_Sources.LeftHand))
            {
                Pulse(0.01f, 150, 750, SteamVR_Input_Sources.LeftHand);
            }
            if (trackpadAction.GetStateDown(SteamVR_Input_Sources.RightHand))
            {
                Pulse(0.01f, 150, 750, SteamVR_Input_Sources.RightHand);
                
            }

            if (shot)
            {
                if (shootTimer > 0)
                {
                    Debug.Log("Zählt");
                    shootTimer -= Time.deltaTime;
                }
                else
                {
                    shot = false;
                }
            }
        }
        public void Pulse(float duration, float frequency,float amplitude, SteamVR_Input_Sources source)
        {
            hapticAction.Execute(0, duration, frequency, amplitude, source);
        }
        public void Vibe1()
        {            
                Pulse(0.01f, 150, 750, SteamVR_Input_Sources.RightHand);     
            
        }
        public void Vibe2()
        {
            Pulse(0.01f, 150, 750, SteamVR_Input_Sources.LeftHand);

        }
        //public void VibeLeft()
        //{            
        //        Pulse(0.01f, 150, 750, SteamVR_Input_Sources.LeftHand);                     
        //}
    }
}
 