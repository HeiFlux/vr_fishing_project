﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Valve.VR.InteractionSystem
{
    public class ButtonControllerRotation : MonoBehaviour
    {

        public bool Go;

        public SpeedRotation SR;

        public float PressTime = 0f;
        public float EndTime = 3f;
        
        public HoverButton hoverButtonStart;
       
        public HoverButton hoverButtonPlus;
        public HoverButton hoverButtonMinus;

        // Use this for initialization
        private void Start()
        {
            GameObject RotationBootMover = GameObject.FindGameObjectWithTag("Motor");
            SR = RotationBootMover.GetComponent<SpeedRotation>();

            hoverButtonStart.onButtonDown.AddListener(OnButtonDownStart);
            
            hoverButtonPlus.onButtonDown.AddListener(OnButtonDownPlus);
            hoverButtonMinus.onButtonDown.AddListener(OnButtonDownMinus);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V) && Go == false)
            {
                Debug.Log("down V one");
                SR.Start = true;
                StartCoroutine(StartBoat());
            }

            



            if (Input.GetKeyDown(KeyCode.V) && Go == true)
            {
                Debug.Log("down V two");
                SR.Start = false;
              
                    Go = false;
             
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                SR.MaxSpeed += 1f;

            }
        }

        IEnumerator StartBoat()
        {
            yield return new WaitForSeconds(EndTime);
            Go = true;

        }


        private void OnButtonDownStart(Hand hand)
        {
            SR.Start = true;

             if (Go == false)
              {
                Debug.Log("down V one");
                SR.Start = true;
                StartCoroutine(StartBoat());
            }

              if (Go == true)
              {
                  SR.Start = false;
                  Go = false;
              }
        }

        private void OnButtonDownStop(Hand hand)
        {
            SR.Start = true;
        }

        private void OnButtonDownPlus(Hand hand)
        {
            SR.MaxSpeed += 1f;
        }

        private void OnButtonDownMinus(Hand hand)
        {
            SR.MaxSpeed -= 1f;
        }





       
    }
}