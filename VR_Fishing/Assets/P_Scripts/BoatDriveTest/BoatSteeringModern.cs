﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Valve.VR.InteractionSystem
{
    public class BoatSteeringModern : MonoBehaviour
    {

        public Rigidbody boat;
        public float speed;
        public GameObject motor;
        public float rotaiton = 3f;

        public LinearMapping left;
        public LinearMapping right;

        private bool LeftGear;
        private bool RightGear;

        public Sticky sticky;
        public GameObject stickyObjekt;

        public Haptics haptics;

        private bool vibed1 = true;
        private bool vibed2 = true;

        public bool canDrive = true;

        public GameObject leftObj;
        public GameObject rightObj;
        public GameObject controlls;


        public Transform leftRestPos;
        public Transform rightRestPos;

        public AudioSource motorAudio;

        private void OnEnable()
        {
            leftObj.transform.position = leftRestPos.transform.position;
            rightObj.transform.position = rightRestPos.transform.position;

            left.value = 0;
            right.value = 0;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
           if(right.value >= 0.2 && right.value <= 0.5 && vibed1)
            {

                haptics.Vibe1();
                vibed1 = false;
            }
            if (right.value >= 0.5 && right.value <= 0.8 && !vibed1)
            {

                haptics.Vibe1();
                vibed1 = true;
            }
            if (right.value >= 0.8 && right.value <= 1 && vibed1)
            {

                haptics.Vibe1();
                vibed1 = false;
            }

            if (left.value >= 0.2 && left.value <= 0.5 && vibed2)
            {

                haptics.Vibe2();
                vibed2 = false;
            }
            if (left.value >= 0.5 && left.value <= 0.8 && !vibed2)
            {

                haptics.Vibe2();
                vibed2 = true;
            }
            if (left.value >= 0.8 && left.value <= 1 && vibed2)
            {

                haptics.Vibe2();
                vibed2 = false;
            }

            if (left.value >= 0.2 || right.value >= 0.2)
            {
                boat.isKinematic = false;
                sticky.enabled = true;
                sticky.gameObject.SetActive(true);

            }



            if (left.value >= 0.2 && right.value >= 0.2)
            {
                

                if (left.value <= 0.8 || right.value <= 0.8)
                {
                    boat.velocity = (transform.forward) * speed * Time.fixedDeltaTime;
                    if (left.value >= 0.2 && right.value >= 0.5)
                    {
                        transform.Rotate(0, -rotaiton  * Time.deltaTime, 0);
                        boat.velocity = (transform.forward) * speed*2 * Time.fixedDeltaTime;
                        motorAudio.volume = 0.05f;


                    }
                    if (right.value >= 0.2 && left.value >= 0.5)
                    {
                        transform.Rotate(0, rotaiton * Time.deltaTime, 0);
                        boat.velocity = (transform.forward) * speed*2 * Time.fixedDeltaTime;
                        motorAudio.volume = 0.05f;

                    }
                } 
                else
                {
                    boat.velocity = (transform.forward) * speed * 3 * Time.fixedDeltaTime;
                    motorAudio.volume = 0.08f;
                }
            }
            else
            {
                if (left.value >= 0.8 && right.value <= 0.2)
                {
                    transform.Rotate(0, rotaiton * 2 * Time.deltaTime, 0);
                    boat.velocity = (transform.forward) * speed * 2 * Time.fixedDeltaTime;
                    motorAudio.volume = 0.05f;

                }
                if (left.value <= 0.2 && right.value >= 0.8)
                {
                    transform.Rotate(0, -rotaiton * 2 * Time.deltaTime, 0);
                    boat.velocity = (transform.forward) * speed * 2 * Time.fixedDeltaTime;
                    motorAudio.volume = 0.05f;

                }
                if (left.value <= 0.2 && right.value <= 0.2)
                {
                    boat.velocity = Vector3.zero;
                    boat.isKinematic = true;
                    sticky.transform.DetachChildren();
                    sticky.enabled = false;
                    sticky.gameObject.SetActive(false);
                    motorAudio.volume = 0.03f;

                }

            }
            


        }

        public void ResetMotor()
        {
            leftObj.transform.position = leftRestPos.transform.position;
            rightObj.transform.position = rightRestPos.transform.position;

            left.value = 0;
            right.value = 0;

            //controlls.SetActive(false);
        }
        protected void LateUpdate()
        {
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
            transform.localPosition = new Vector3(transform.localPosition.x, 0.46f, transform.localPosition.z);
        }
    }

}
