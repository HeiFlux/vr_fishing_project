﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{

    public class BoatSteeringTest : MonoBehaviour {

        public Rigidbody boat;
        public float speed;
        public GameObject motor;
        public float rotaiton = 3f;

        public LinearMapping steerMapping;
        public LinearMapping speedMapping;
        //public Gun;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (speedMapping.value >= 0.1 && speedMapping.value <= 0.3)
            {
                boat.velocity = (transform.forward) * speed/2 * Time.fixedDeltaTime;
            }
            if (speedMapping.value <= 0)
            {
                boat.velocity = Vector3.zero;
            }
            if (speedMapping.value >= 0.6 && speedMapping.value <= 0.7)
            {
                boat.velocity = (transform.forward) * speed * Time.fixedDeltaTime;
            }
            if (speedMapping.value >= 0.7 )
            {
                boat.velocity = (transform.forward) * speed *2 * Time.fixedDeltaTime;
            }

            

            if(steerMapping.value >= 0.6)
            {
                transform.Rotate(0, rotaiton * Time.deltaTime, 0);
            }
            if (steerMapping.value >= 0.9)
            {
                transform.Rotate(0, rotaiton*2 * Time.deltaTime, 0);
            }
            if (steerMapping.value <= 0.4)
            {
                transform.Rotate(0, -rotaiton * Time.deltaTime, 0);
            }
            if (steerMapping.value <= 0.1)
            {
                transform.Rotate(0, -rotaiton*2 * Time.deltaTime, 0);
            }
        }
    }
}
