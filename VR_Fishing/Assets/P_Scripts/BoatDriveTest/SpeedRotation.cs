﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
   
    public class SpeedRotation : MonoBehaviour
    {

        public float BoatSpeed = 0f;
        public float MaxSpeed = 10f;
        public float MinSpeed = 0f;
        public float Steigung = 2f;
       

        public bool Start;


        public void Update()
        {
            if (Start == true) 
            {
                transform.Rotate(Vector3.up, -BoatSpeed * Time.deltaTime);

                BoatSpeed += Steigung * Time.deltaTime;

                if(BoatSpeed > MaxSpeed)
                {
                    BoatSpeed = MaxSpeed;
                }
            }

            if (Start == false)
            {
                MaxSpeed = 10f;

                transform.Rotate(Vector3.up, -BoatSpeed * Time.deltaTime);

                BoatSpeed -= Steigung * Time.deltaTime;

                if (BoatSpeed < MinSpeed)
                {
                    BoatSpeed = MinSpeed;
                }
            }


        }
       
    }
}