﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatStateController : MonoBehaviour {

    public GameObject boat;
    public GameObject fish;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BoatStateSwitch_On()
    {
        Debug.Log("Boot kann fahren");
        boat.SetActive(true);
        fish.SetActive(false);

    }
    public void BoatStateSwitch_Off()
    {
        Debug.Log("Boot kann nicht fahren");
        boat.SetActive(false);
        fish.SetActive(true);

    }
}
