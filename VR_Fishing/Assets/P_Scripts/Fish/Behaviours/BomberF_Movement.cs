﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BomberF_Movement : MonoBehaviour , IObject
{

    public Rigidbody ownRb;
    public Animator ownAnim;
    private GameObject attackpoint;

    public int jumpForce;
    public float speed;
    public bool attacking;

    public LootChances lootChances;

    


    void Awake()
    {
        lootChances = FindObjectOfType<LootChances>();
        
    }
        // Use this for initialization
    void OnEnable()
    {
        
        ownRb.isKinematic = false;
        ownRb.useGravity = true;
        //attackpoint = GameObject.Find("TargetPoint");
        ownRb.AddForce(0, jumpForce, 0);
        Invoke("BeginAttack", 1f);
        ownAnim.SetTrigger("Start");


    }
    void BeginAttack()
    {
        
        attacking = true;
        ownRb.isKinematic = true;
        ownRb.useGravity = false;
        ownAnim.SetBool("Attacking", true);
    }

    // Update is called once per frame
    void Update()
    {
        attackpoint = GameObject.Find("TargetPoint");
        transform.LookAt(attackpoint.transform.position);
        if (attacking)
        {
            float step = speed * Time.deltaTime;

            // Move our position a step closer to the target.
            transform.position = Vector3.MoveTowards(transform.position, attackpoint.transform.position, step);
        }
    }
    void OnDisable()
    {
        gameObject.SetActive(false);

        EventObjectDeactivated(this);
        attackpoint = null;
        lootChances.BomberfischKilled();
    }

    // FISCH SPAWNER OBJECTPOOLINGOBJECT muss auf dem Object sein das im Pool is und zerstört wird(Disabled)
    private Action<IObject> _eventObjectDeactivated = delegate { };
    public Action<IObject> EventObjectDeactivated
    {
        get
        {
            return _eventObjectDeactivated;
        }

        set
        {
            _eventObjectDeactivated = value;
        }
    }

    public GameObject _Objekt
    {
        get
        {
            return gameObject;
        }
    }

    public void ActivateObject()
    {
    }


    /*
    private void OnMouseDown()
    {
        GetHit();
    }


    private void GetHit()
    {
        gameObject.SetActive(false);

        EventObjectDeactivated(this);

    }*/

}