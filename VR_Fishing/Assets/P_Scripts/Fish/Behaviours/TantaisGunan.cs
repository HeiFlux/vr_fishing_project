﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TantaisGunan : MonoBehaviour {

    public GameObject Gundes;

    public bool Gunisan;

    public GameObject OldTant;
    public GameObject NewTant;
	// Use this for initialization
	void Start () {
        //Gundes = GameObject.Find("Spawner_Shot");
	}
	
	// Update is called once per frame
	void Update () {
        if (Gundes.activeSelf)
        {
            Gunisan = true;
        }

		if(Gunisan == true)
        {
            OldTant.SetActive(false);
            NewTant.SetActive(true);
        }


	}
}
