﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HomingJumpF_Movement : MonoBehaviour , IObject {

    Vector3 startingPoint;

    public GameObject attackPoint;

    Vector3 conPre;
    Vector3 controlPoint;

    public bool attacking = false;
    public float attackSpeed;

    public float offset = 10f;



    public float count = 0.0f;

    public LootChances lootChances;

    void Awake()
    {
        lootChances = FindObjectOfType<LootChances>();
    }

   void OnEnable()
   {

        attacking = false;
        attackPoint = GameObject.Find("TargetPoint");
       
        Invoke("SetStartingPosi", 0.2f);     
  
   }


    void Update()
    {
        if (count < 1.0f && attacking)
        {
            count += attackSpeed * Time.deltaTime / 2;

            Vector3 m1 = Vector3.Lerp(startingPoint, controlPoint, count);
            Vector3 m2 = Vector3.Lerp(controlPoint, attackPoint.transform.position, count);
            this.transform.position = Vector3.Lerp(m1, m2, count);
        }
        transform.LookAt(attackPoint.transform.position);

    }
    void OnDisable()
    {

        gameObject.SetActive(false);

        EventObjectDeactivated(this);

        attackPoint = null;        
        
        lootChances.HomingFischKilled();
    }

    void SetStartingPosi()
    {
        
        count = 0.0f;
        startingPoint = this.transform.position;
        //transform.LookAt(attackPoint.transform.position);
        Invoke("SetControlPoint", 0.2f);
        attackPoint = GameObject.Find("TargetPoint");
        controlPoint = GetPoint();

        attacking = true;
        
    }


    Vector3 GetPoint()
    {
        Vector3 pos1 = startingPoint;
        Vector3 pos2 = attackPoint.transform.position;

        Vector3 dir = (pos1 - pos2).normalized;

        Vector3 perpDir = Vector3.Cross(dir, Vector3.right);

        Vector3 midPoint = (pos1 + pos2) / 2f;
        //get the offset point
        //This is the point you're looking for.
        Vector3 offsetPoint = midPoint + (perpDir * offset);

        return offsetPoint;

        //Formel die den Mittelpunkt zwischen Startpunkt und ziel berechnet, darauf hin wird noch eine Höhe hinzu gefügt
        //conPre = new Vector3(startingPoint.x + (attackPoint.transform.position.x - startingPoint.x) / 2, startingPoint.y + (attackPoint.transform.position.y - startingPoint.y) / 2, startingPoint.z + (attackPoint.transform.position.z - startingPoint.z) / 2);
        //controlPoint = new Vector3(conPre.x, conPre.y + 10f, conPre.y);
    }

    // FISCH SPAWNER OBJECTPOOLINGOBJECT muss auf dem Object sein das im Pool is und zerstört wird(Disabled)
    private Action<IObject> _eventObjectDeactivated = delegate { };
    public Action<IObject> EventObjectDeactivated
    {
        get
        {
            return _eventObjectDeactivated;
        }

        set
        {
            _eventObjectDeactivated = value;
        }
    }

    public GameObject _Objekt
    {
        get
        {
            return gameObject;
        }
    }

    public void ActivateObject()
    {
    }
}
