﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Jumpifisch : MonoBehaviour, IObject
{

    public Rigidbody ownRb;
    public Animator ownAnim;

    private GameObject attackpoint;

    public int jumpForce;
    public int turnSpeed;

    private float height;


    public LootChances lootChances;

    void Awake()
    {
        lootChances = FindObjectOfType<LootChances>();
    }


    void OnEnable()
    {
        ownRb.isKinematic = false;
        ownRb.useGravity = true;
        height = transform.position.y;
        attackpoint = GameObject.Find("TargetPoint");
        transform.LookAt(attackpoint.transform.position);
        //ownRb.AddForce(0, jumpForce, 0);
        ownRb.AddForce(transform.forward * 10);
        Invoke("BeginAttack", 1f);
        //ownAnim.SetTrigger("Start");





    }
    void Update()
    {


        transform.LookAt(attackpoint.transform.position);
        //transform.rotation = Quaternion.LookRotation(newDir);
        if (transform.position.y <= height)
        {
            ownRb.isKinematic = true;
            ownRb.isKinematic = false;
            ownRb.AddForce(0, jumpForce, 0);
            ownRb.AddForce(transform.forward * 50);

        }
    }
    void OnDisable()
    {
        gameObject.SetActive(false);

        EventObjectDeactivated(this);

        attackpoint = null;
        lootChances.JumpyFischKilled();

    }

    // FISCH SPAWNER OBJECTPOOLINGOBJECT muss auf dem Object sein das im Pool is und zerstört wird(Disabled)
    private Action<IObject> _eventObjectDeactivated = delegate { };
    public Action<IObject> EventObjectDeactivated
    {
        get
        {
            return _eventObjectDeactivated;
        }

        set
        {
            _eventObjectDeactivated = value;
        }
    }

    public GameObject _Objekt
    {
        get
        {
            return gameObject;
        }
    }

    public void ActivateObject()
    {
    }
}
