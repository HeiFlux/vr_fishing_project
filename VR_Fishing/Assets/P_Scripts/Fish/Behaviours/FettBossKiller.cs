﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FettBossKiller : MonoBehaviour {

     public GameObject z1;
     public GameObject z2;
     public GameObject z3;
     public GameObject z4;
     public GameObject z5;
     public GameObject z6;
     public GameObject z7;
     public GameObject z8;
     public GameObject zhorn;


       public  bool b_z1;
       public  bool b_z2;
       public  bool b_z3;
       public  bool b_z4;
       public  bool b_z5;
       public  bool b_z6;
       public  bool b_z7;
       public  bool b_z8;
       public  bool b_zhorn;

    public LootChances lootChances;

    void Awake()
    {
        lootChances = FindObjectOfType<LootChances>();

    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!z1.activeSelf)
        {
            b_z1 = true;
        }

        if (!z2.activeSelf)
        {
            b_z2 = true;
        }

        if (!z3.activeSelf)
        {
            b_z3 = true;
        }

        if (!z4.activeSelf)
        {
            b_z4 = true;
        }

        if (!z5.activeSelf)
        {
            b_z5 = true;
        }

        if (!z6.activeSelf)
        {
            b_z6 = true;
        }

        if (!z7.activeSelf)
        {
            b_z7 = true;
        }

        if (!z8.activeSelf)
        {
            b_z8 = true;
        }

        if (!zhorn.activeSelf)
        {
            b_zhorn = true;
        }

        if(b_z1  && b_z2  && b_z3 && b_z4  && b_z5  && b_z6  && b_z7  && b_z8  && b_zhorn )
        {
            lootChances.Boss_FettiKilled();
            gameObject.SetActive(false);
        }
    }
}
