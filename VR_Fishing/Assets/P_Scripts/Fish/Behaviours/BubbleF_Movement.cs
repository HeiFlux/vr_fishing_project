﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleF_Movement : MonoBehaviour {

    public Rigidbody ownRb;
    public Animator ownAnim;

    private GameObject attackpoint;

    public int jumpForce;

    public float speed;

    public bool attacking;

	// Use this for initialization
	void OnEnable ()
    {
        ownRb.isKinematic = false;
        ownRb.useGravity = true;
        attackpoint = GameObject.Find("TargetPoint");
        ownRb.AddForce(0, jumpForce, 0);
        Invoke("BeginAttack", 1f);
        ownAnim.SetTrigger("Start");


    }
    void BeginAttack()
    {
        attacking = true;
        ownRb.isKinematic = true;
        ownRb.useGravity = false;
        ownAnim.SetBool("Attacking", true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(attackpoint.transform.position);
        if (attacking)
        {
            float step = speed * Time.deltaTime;

            // Move our position a step closer to the target.
            transform.position = Vector3.MoveTowards(transform.position, attackpoint.transform.position, step);
        }
    }
    void OnDisable()
    {
        attackpoint = null;
    }
}
