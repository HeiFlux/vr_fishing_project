﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Kurven : MonoBehaviour
{

    Vector3 startingPoint;
    public GameObject attackPoint;

    Vector3 conPre;
    Vector3 controlPoint;



    float count = 0.0f;

    void OnEnable()
    {
        count = 0.0f;
        startingPoint = transform.position;
        attackPoint =  GameObject.Find("TargetPoint");

        //Formel die den Mittelpunkt zwischen Startpunkt und ziel berechnet, darauf hin wird noch eine Höhe hinzu gefügt
        conPre = new Vector3(startingPoint.x + (attackPoint.transform.position.x - startingPoint.x) / 2, startingPoint.y + (attackPoint.transform.position.y - startingPoint.y) / 2, startingPoint.z + (attackPoint.transform.position.z - startingPoint.z) / 2);
        controlPoint = new Vector3(conPre.x,conPre.y + 10f,conPre.y);        

    }


    void Update()
    {
        if (count < 1.0f)
        {
            count += 0.5f * Time.deltaTime/2;

            Vector3 m1 = Vector3.Lerp(startingPoint, controlPoint, count);
            Vector3 m2 = Vector3.Lerp(controlPoint, attackPoint.transform.position, count);
            this.transform.position = Vector3.Lerp(m1, m2, count);
        }
    }
}