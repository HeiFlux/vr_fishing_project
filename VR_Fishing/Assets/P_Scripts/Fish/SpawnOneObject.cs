﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SpawnOneObject : MonoBehaviour {

    public GameObject OneTimeFish;
    
    public void SpawnObject()
    {

        Instantiate(OneTimeFish, transform.position, transform.rotation);

        
    }
}
