﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishJump2 : MonoBehaviour {

    private float speed = 11f;
    private float left = 3f;
    private Rigidbody rb;
    private bool moving = false;

    public float t = 1f;
    public float maxTime = 1f;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(Random.Range(-1f, 1f), Random.Range(speed - 1, speed + 1), Random.Range(-1f, 1f));
        moving = true;

        if (moving)
        {
            // when the cube has moved over 1 second report it's position
            t = t + Time.deltaTime;
            if (t > maxTime)
            {
                speed = 0f;
                left = 0f;
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Fish")
        {
            Destroy(gameObject);
        }
    }
}
