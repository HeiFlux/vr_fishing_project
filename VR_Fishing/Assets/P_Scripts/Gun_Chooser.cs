﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Chooser : MonoBehaviour
{

    public GameObject gun;
    public GameObject rapidGun;
    public GameObject shotGun;

    public GameObject buttons;

    public bool ScriptedStart = false;




    // Use this for initialization
    void Start()
    {
        if (ScriptedStart)
        {
            gun.SetActive(false);
            rapidGun.SetActive(false);
            shotGun.SetActive(false);
        }
        else
        {
            gun.SetActive(true);
            rapidGun.SetActive(false);
            shotGun.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("o"))
        {
            buttons.SetActive(true);
        }
    }

    public void SwitchGun()
    {
        if (gun)
        {
            gun.SetActive(false);
            rapidGun.SetActive(true);
            shotGun.SetActive(false);
        }
        else if (rapidGun)
        {
            gun.SetActive(false);
            rapidGun.SetActive(false);
            shotGun.SetActive(true);
        }
        else if (shotGun)
        {
            gun.SetActive(true);
            rapidGun.SetActive(false);
            shotGun.SetActive(false);
        }
    }
    public void SetGun()
    {
        gun.SetActive(true);
        rapidGun.SetActive(false);
        shotGun.SetActive(false);
    }
    public void SetRapid()
    {
        gun.SetActive(false);
        rapidGun.SetActive(true);
        shotGun.SetActive(false);
    }
    public void SetShot()
    {
        gun.SetActive(false);
        rapidGun.SetActive(false);
        shotGun.SetActive(true);
    }

    public void TurnOffGuns()
    {
        gun.SetActive(false);
        rapidGun.SetActive(false);
        shotGun.SetActive(false);
    }
}
