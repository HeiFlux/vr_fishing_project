﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour {

	// Use this for initialization
	private void OnEnable () {
        Invoke("Destroy", 0.9f);
    }
	
	// Update is called once per frame
	private void Destroy () {
        gameObject.SetActive(false);
	}

    private void OnDisable()
    {
        CancelInvoke();
    }
}
